all: install_dependencies run_scrape
	echo "All done!"

install_dependencies:
	pip install -r requirements.txt
	echo "Dependencies installed."

run_scrape:
	python3 src/scrape.py