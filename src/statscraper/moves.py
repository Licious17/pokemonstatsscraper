class MoveSource:
    move_name: str
    
    def __init__(self, move_name: str):
        self.move_name = move_name
        
    def toJson(self) -> str:
        pass
    
class LevelMoveSource(MoveSource):
    
    level: int
    
    def __init__(self, move_name: str, level: int):
        self.move_name = move_name 
        self.level = level
        
    def toJson(self) -> str:
        return '%s:%s' % (self.level, self.move_name)
    
    
class TutorMoveSource(MoveSource):
        
    def toJson(self) -> str:
        return 'tutor:%s' % self.move_name
    
class TechnicalDiskMoveSource(MoveSource):
        
    def toJson(self) -> str:
        return 'tm:%s' % self.move_name      
    
class EggMoveSource(MoveSource):
        
    def toJson(self) -> str:
        return 'egg:%s' % self.move_name   
    
class FormChangeSource(MoveSource):
    
    def toJson(self) -> str:
        return 'form_change:%s' % self.move_name  