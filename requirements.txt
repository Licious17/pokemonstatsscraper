# Package           Version
tqdm            ==  4.63.1
beautifulsoup4  ==  4.9.3
requests        ==  2.26.0
asyncio         ==  3.4.3
aiopokeapi      ==  0.1.4
