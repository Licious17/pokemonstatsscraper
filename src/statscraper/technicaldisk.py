from enum import Enum
from typing import Any, Dict


class DiskType(Enum):
    TM = 'tm'
    HM = 'hm'
    TR = 'tr'

class TechnicalDisk:
    move_name: str
    number: int
    generation: int
    type: DiskType
    
    def __init__(self, move_name: str, number: int, generation: int, type: DiskType) -> None:
        self.move_name = move_name
        self.number = number
        self.generation = generation
        self.type = type
    
    def asDict(self) -> Dict[str, Any]:
        return {
            'move': self.move_name,
            'number': self.number,
            'generation': self.generation,
            'type': self.type.value
        }
    
    def pretty_name(self) -> str:
        return self.type.value + ':' + str(self.number)