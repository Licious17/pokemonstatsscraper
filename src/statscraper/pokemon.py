from json import JSONEncoder
import re
from typing import Any, Dict, List, Optional
from statlike import *
from moves import *
from evolution import *

class StatHolder:
    name: str
    primary_type: ElementalType
    secondary_type: Optional[ElementalType] 
    abilities: List[Ability]
    base_stats: Dict[BaseStat, int]
    catch_rate: int
    male_ratio: float
    shoulder_mountable: bool
    base_experience_yield: int
    base_friendship: int
    ev_yield: Dict[BaseStat, int]
    experience_group: ExperienceGroup
    egg_cycles: int
    egg_groups: List[EggGroup]
    moves: List[MoveSource]
    labels: List[str]
    height: float
    weight: float
    pre_evolution: str
    evolutions: List[Evolution]
    
    def __init__(self):
        self.secondary_type = None
        self.abilities = list()
        self.base_stats = dict()
        for base_stat in BaseStat:
            self.base_stats[base_stat] = 1    
            self.shoulder_mountable = False
        self.ev_yield = dict()
        self.egg_groups = list()
        self.moves = list()
        self.labels = list()
        self.pre_evolution = None
        self.evolutions = list()

    def createPokedexEntry(self, entry_number: int) -> str:
        pass
    
    def pretty_name(self) -> str:
        pass

    def asDict(self) -> Dict[str, Any]:
        output = { 'primaryType': self.primary_type.value }
        if not self.secondary_type is None:
            output['secondaryType'] = self.secondary_type.value
        output['abilities'] = list(map(lambda ability: (ability.name if not ability.is_hidden else 'h:%s' % ability.name), self.abilities))
        output['baseStats'] = dict(map(lambda kv: (kv[0].value, kv[1]), self.base_stats.items()))
        output['catchRate'] = self.catch_rate
        output['maleRatio'] = self.male_ratio
        output['shoulderMountable'] = self.shoulder_mountable
        output['baseExperienceYield'] = self.base_experience_yield
        output['baseFriendship'] = self.base_friendship
        output['evYield'] = dict(map(lambda kv: (kv[0].value, kv[1]), self.ev_yield.items()))
        output['baseExperienceYield'] = self.base_experience_yield
        output['experienceGroup'] = self.experience_group.value
        output['eggCycles'] = self.egg_cycles
        output['eggGroups'] = list(map(lambda eggGroup: eggGroup.value, self.egg_groups))
        moves = list(map(lambda move: move.toJson(), self.moves))
        numerical_entries = list(filter(lambda x: re.search("[0-9]", x) is not None, moves))
        text_entries = list(filter(lambda x: x not in numerical_entries, moves))
        text_entries.sort()
        numerical_entries.sort(reverse = False, key = lambda x: int(x.split(':')[0]))
        moves = []
        for numerical_entry in numerical_entries:
            moves.append(numerical_entry)
        for text_entry in text_entries:
            moves.append(text_entry)
        output['moves'] = moves
        output['labels'] = sorted(self.labels, key=str.lower)
        output['pokedex'] = [
            self.createPokedexEntry(1),
            self.createPokedexEntry(2)
        ]
        output['height'] = self.height
        output['weight'] = self.weight
        output['baseScale'] = 1.0
        output['hitbox'] = {
            'width': 1.0,
            'height': 1.0,
            'fixed': False
        }
        if not self.pre_evolution is None:
            output['preEvolution'] = self.pre_evolution
        evolution_output = []
        for evolution in map(lambda evolution: evolution.asDict(), self.evolutions):
            if evolution not in evolution_output:
                evolution_output.append(evolution)
        output['evolutions'] = evolution_output
        return output 

class Form(StatHolder):
    species_name: str
    
    def createPokedexEntry(self, entry_number: int) -> str:
        return 'pokemoncobbled.form.%s-%s.desc%s' % (self.species_name, self.name, entry_number)
    
    def asDict(self) -> Dict[str, Any]:
        output = {
            'name' : self.name
        }
        output.update(super().asDict())
        return output 
    
    def pretty_name(self) -> str:
        return '%s-%s' % (self.name, self.species_name)

class Species(StatHolder):
    
    national_pokedex_number: int
    forms: List[Form]
    
    def __init__(self):
        super().__init__()
        self.forms = list()
        
    def createPokedexEntry(self, entry_number: int) -> str:
        return 'pokemoncobbled.species.%s.desc%s' % (self.name, entry_number)
    
    def asDict(self):
        output = {
            'name': self.name,
            'nationalPokedexNumber': self.national_pokedex_number
        }
        output.update(super().asDict())
        form_exports = list()
        for form_export in map(lambda form: form.asDict(), self.forms):
            for (key, value) in output.items():
                if key != 'name' and key in form_export and form_export[key] == value:
                    del form_export[key]
            form_exports.append(form_export)
        output['forms'] = form_exports
        return output 
    
    def pretty_name(self) -> str:
        return self.name    

