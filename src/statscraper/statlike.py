from enum import Enum

class ElementalType(Enum):
    NORMAL = 'normal'
    FIRE = 'fire'
    WATER = 'water'
    ELECTRIC = 'electric'
    GRASS = 'grass'
    ICE = 'ice'
    FIGHTING = 'fighting'
    POISON = 'poison'
    GROUND = 'ground'
    FLYING = 'flying'
    PSYCHIC = 'psychic'
    BUG = 'bug'
    ROCK = 'rock'
    GHOST = 'ghost'
    DRAGON = 'dragon'
    DARK = 'dark'
    STEEL = 'steel'
    FAIRY = 'fairy'

class BaseStat(Enum):
    HP = 'hp'
    ATTACK = 'attack'
    DEFENSE = 'defence'
    SPECIAL_ATTACK = 'special_attack'
    SPECIAL_DEFENSE = 'special_defence'
    SPEED = 'speed'

class ExperienceGroup(Enum):
    SLOW = 'slow'
    MEDIUM_SLOW = 'mediumslow'
    MEDIUM_FAST = 'mediumfast'
    FAST = 'fast'
    ERRATIC = 'erratic'
    FLUCTUATING = 'fluctuating'

class EggGroup(Enum):
    MONSTER = 'monster'
    WATER1 = 'water1'
    BUG = 'bug'
    FLYING = 'flying'
    GROUND = 'field'
    FAIRY = 'fairy'
    PLANT = 'grass'
    HUMANSHAPE = 'human_like'
    WATER3 = 'water_3'
    MINERAL = 'mineral'
    INDETERMINATE = 'amorphous'
    WATER2 = 'water_2'
    DITTO = 'ditto'
    DRAGON = 'dragon'
    NO_EGGS = 'undiscovered'
    
class Ability:
    name: str
    is_hidden: bool
    
    def __init__(self, name: str, is_hidden: bool):
        self.name = name
        self.is_hidden = is_hidden 
             