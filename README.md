# PokemonStatsScraper

Scrapes Pokémon data from the internet and produces a set of JSON files.
## How To Use

<u>To generate the stat files:</u>
* Place any existing JSONs you'd like to append data to in the appending folder, otherwise procede
* Execute `make`
* All generated or appended files will be found under the ouput folder

<u>If you do not have `make` installed, these are the corresponding commands:</u>
* `pip install -r requirements.txt`
* `python3 src/scrape.py`

## Output
The existing/generated JSONs in the `output` folder will be overwritten with the results.
