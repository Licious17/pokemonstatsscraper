from enum import Enum
from typing import Any, Dict, List

from statlike import ElementalType

class EvolutionLike:
    variant: str

class EvolutionRequirement(EvolutionLike):
    
    def asDict(self) -> Dict[str, Any]:
        return {
            'variant': self.variant
        }
    
class IntRequirement(EvolutionRequirement):
    
    variant: str = 'battle_critical_hits'
    amount: int
    amount_property_name: str
    
    def __init__(self, variant: str, amount: int, amount_property_name: str = 'amount'):
        super().__init__()
        self.variant = variant
        self.amount = amount
        self.amount_property_name = amount_property_name
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output[self.amount_property_name] = self.amount
        return output   
    
class BiomeRequirement(EvolutionRequirement):
    
    variant: str = 'biome'
    biome: str
    
    def __init__(self, biome: str):
        super().__init__()
        self.biome = biome
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['biomeCondition'] = self.biome
        return output
    
class TimeRangeRequirement(EvolutionRequirement):
    
    variant: str = 'time_range'
    range: str
    
    def __init__(self, range: str):
        super().__init__()
        self.range = range
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['range'] = self.range
        return output
    
class HeldItemRequirement(EvolutionRequirement):
    
    variant: str = 'held_item'
    item: str
    
    def __init__(self, item: str):
        super().__init__()
        self.item = item
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['item'] = self.item
        return output
    
class StatRatio(Enum):
    ATTACK_HIGHER = 'ATTACK_HIGHER'
    EQUAL = 'EQUAL'
    DEFENCE_HIGHER = 'DEFENCE_HIGHER' 
    
class StatRatioRequirement(EvolutionRequirement):
    
    variant: str = 'attack_defence_ratio'
    ratio: StatRatio
    
    def __init__(self, ratio: StatRatio):
        super().__init__()
        self.ratio = ratio
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['ratio'] = self.ratio.value
        return output 
    
class PartyMemberRequirement(EvolutionRequirement):
    
    variant: str = 'party_member'
    property: str
    contains: bool
    
    def __init__(self, property: str, contains: bool = True):
        super().__init__()
        self.property = property
        self.contains = contains
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['contains'] = self.contains
        output['target'] = self.property
        return output  
    
class PropertiesRequirement(EvolutionRequirement):
    
    variant: str = 'properties'
    property: str
    
    def __init__(self, property: str):
        super().__init__()
        self.property = property
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['target'] = self.property
        return output
    
class MoveRequirement(EvolutionRequirement):
    
    variant: str = 'has_move'
    move: str
    
    def __init__(self, move: str):
        super().__init__()
        self.move = move
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['move'] = self.move
        return output         

class MoveTypeRequirement(EvolutionRequirement):
    
    variant: str = 'has_move_type'
    type: ElementalType
    
    def __init__(self, type: ElementalType):
        super().__init__()
        self.type = type
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['type'] = self.type.value
        return output
    
class RainRequirement(EvolutionRequirement):
    
    variant: str = 'weather'
    
    def asDict(self) -> Dict[str, Any]:
        output = super().asDict()
        output['isRaining'] = True
        return output      

class Evolution(EvolutionLike):
    source_species: str
    resulting_species: str
    resulting_property: str
    requirements: List[EvolutionRequirement]
    consume_held_item: bool
    
    def __init__(self, source_species: str, resulting_species: str, resulting_property: str):
        self.source_species = source_species
        self.resulting_species = resulting_species
        self.resulting_property = resulting_property
        self.requirements = list()
        self.consume_held_item = False
        
    def asDict(self) -> Dict[str, Any]:
        return {
            'id': '%s_%s' % (self.source_species, self.resulting_species),
            'variant': self.variant,
            'result': self.resulting_property,
            'consumeHeldItem': self.consume_held_item,
            'learnableMoves': [],
            'requirements': list(map(lambda requirement: requirement.asDict(), self.requirements))
        }
        
class PassiveEvolution(Evolution):
    
    def __init__(self, source_species: str, resulting_species: str, resulting_property: str, name_passive: bool = False):
        super().__init__(source_species, resulting_species, resulting_property)
        self.variant = 'passive' if name_passive else 'level_up'
        
class ItemInteractionEvolution(Evolution):
    
    item: str
    
    def __init__(self, source_species: str, resulting_species: str, resulting_property: str, item: str):
        super().__init__(source_species, resulting_species, resulting_property)
        self.variant = 'item_interact'
        self.item = item
        
    def asDict(self) -> Dict[str, Any]:
        export = super().asDict()
        export['requiredContext'] = self.item
        return export
    
class TradeEvolution(Evolution):
    
    trade_partner: str
    
    def __init__(self, source_species: str, resulting_species: str, resulting_property: str, trade_partner: str = None):
        super().__init__(source_species, resulting_species, resulting_property)
        self.variant = 'trade'
        self.trade_partner = trade_partner
        
    def asDict(self) -> Dict[str, Any]:
        export = super().asDict()
        export['requiredContext'] = self.trade_partner
        return export    
    
class BlockInteractionEvolution(Evolution):
    
    block: str
    
    def __init__(self, source_species: str, resulting_species: str, resulting_property: str, block: str):
        super().__init__(source_species, resulting_species, resulting_property)
        self.variant = 'block_click'
        self.block = block
        
    def asDict(self) -> Dict[str, Any]:
        export = super().asDict()
        export['block'] = self.block
        return export