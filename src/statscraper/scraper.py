import asyncio
from dis import dis
import json
from pathlib import Path
from typing import Iterable
import aiopoke

from pokemon import *
from statlike import *
from moves import *
from evolution import *
from technicaldisk import *

GEN_1_RANGE = range(1, 152)
GEN_2_RANGE = range(152, 253)
GEN_3_RANGE = range(253, 387)
GEN_4_RANGE = range(387, 494)
GEN_5_RANGE = range(494, 650)
GEN_6_RANGE = range(650, 722)
GEN_7_RANGE = range(722, 810)
GEN_8_RANGE = range(810, 899)
# Not quite sure what exactly this game is meant to be but it has unique Pokémon
LA_RANGE = range(899, 906)

# The national pokedex number range to scrape data from, ranges are exclusive so if you want to scrape 1 through 10 the 2nd param should be 11
SCRAPE_RANGE = range(GEN_1_RANGE.start, LA_RANGE.stop)
 # Form suffixes that come along the Pokémon name, these need to be wiped
FORM_SUFFIXES = ['normal', 'altered', 'land', 'red-striped', 'incarnate', 'ordinary', 'standard', 'aria', 'shield', 'male', '50', 'average', 'midday', 'baile', 'red-meteor', 'disguised', 'solo', 'amped', 'full-belly', 'single-strike']

def clean_species_name(pokemon_name: str, is_base: bool = False) -> str:
    if is_base:
        for suffix in FORM_SUFFIXES:
            if pokemon_name.endswith('-' + suffix):
                pokemon_name = pokemon_name.replace('-' + suffix, '')
                break
            else: 
                pass
    return pokemon_name.replace('-', '').replace("'", '').replace('_', '').replace('.', '').replace(' ', '')

def resolve_types(stat_holder: StatHolder, aio_pokemon: aiopoke.Pokemon):
    for type in aio_pokemon.types:
        actual_type = ElementalType[type.type.name.upper()]
        if type.slot == 1:
            stat_holder.primary_type = actual_type
        elif type.slot == 2:
            stat_holder.secondary_type = actual_type
        else:
            raise IndexError('Received invalid type slot %s for the elemental type "%s"' % (type.slot, actual_type.value))
        
def resolve_abilities(stat_holder: StatHolder, aio_pokemon: aiopoke.Pokemon):
    for ability in aio_pokemon.abilities:
        stat_holder.abilities.append(Ability(ability.ability.name.replace('-', ''), ability.is_hidden is True))

def resolve_stat_details(stat_holder: StatHolder, aio_pokemon: aiopoke.Pokemon):
    for stat in aio_pokemon.stats:
        actual_stat = BaseStat[stat.stat.name.replace('-', '_').upper()]
        if stat.effort > 0:
            stat_holder.ev_yield[actual_stat] = stat.effort
        stat_holder.base_stats[actual_stat] = stat.base_stat

def resolve_experience_group(stat_holder: StatHolder, aio_species: aiopoke.PokemonSpecies):
    growth_rate_id = aio_species.growth_rate.id
    if growth_rate_id == 1:
        stat_holder.experience_group = ExperienceGroup.SLOW
    elif growth_rate_id == 2:
        stat_holder.experience_group = ExperienceGroup.MEDIUM_FAST
    elif growth_rate_id == 3:
        stat_holder.experience_group = ExperienceGroup.FAST
    elif growth_rate_id == 4:
        stat_holder.experience_group = ExperienceGroup.MEDIUM_SLOW
    elif growth_rate_id == 5:
        stat_holder.experience_group = ExperienceGroup.ERRATIC
    elif growth_rate_id == 6:
        stat_holder.experience_group = ExperienceGroup.FLUCTUATING
    else:
        raise IndexError('Received invalid experience group with the ID %s named "%s"' % (growth_rate_id, aio_species.growth_rate.name))

def resolve_egg_groups(stat_holder: StatHolder, aio_species: aiopoke.PokemonSpecies): 
    for egg_group in aio_species.egg_groups:
        stat_holder.egg_groups.append(EggGroup[egg_group.name.replace('-', '_').upper()])  

def clean_move_name(move_name: str) -> str:
    return move_name.lower().replace('-', '').replace("'", '').replace('_', '').replace('.', '').replace(' ', '').replace('vicegrip', 'visegrip')
        
def resolve_moves(stat_holder: StatHolder, aio_pokemon: aiopoke.Pokemon):
    for move in aio_pokemon.moves:
        move_name = clean_move_name(move.move.name)
        # Find the most recent implementation of this move in a Pokémon learnset
        latest_version_group_id = max(move.version_group_details, key=lambda detail: detail.version_group.id).version_group.id
        # A move in the latest generation may have more then 1 learn source, we want to keep all of them
        for detail in filter(lambda detail: detail.version_group.id == latest_version_group_id, move.version_group_details):
            learn_type_id = detail.move_learn_method.id
            if learn_type_id == 1:
                stat_holder.moves.append(LevelMoveSource(move_name, detail.level_learned_at))
            elif learn_type_id == 2:
                stat_holder.moves.append(EggMoveSource(move_name))
            # Tutor or one off light ball gimmick for Pichu volt tackle
            elif learn_type_id == 3 or learn_type_id == 6:
                stat_holder.moves.append(TutorMoveSource(move_name))
            elif learn_type_id == 4:
                stat_holder.moves.append(TechnicalDiskMoveSource(move_name))
            # General form change or zygarde cube which is essentially a form change as well in the context of Cobbled
            elif learn_type_id == 10 or learn_type_id == 11:
                stat_holder.moves.append(FormChangeSource(move_name))
            else:
                raise IndexError('Tried to process invalid learn type for %s ("%s" #%s)' % (move_name, detail.move_learn_method.name, learn_type_id))

def resolve_labels(stat_holder: StatHolder, aio_species: aiopoke.PokemonSpecies):
    if aio_species.is_baby:
        stat_holder.labels.append('baby')
    if aio_species.is_legendary:
        stat_holder.labels.append('legendary')
    if aio_species.is_mythical:
        stat_holder.labels.append('mythical')
    stat_holder.labels.append('gen%s' % aio_species.generation.id)
    
def clean_item_name(item: aiopoke.Item) -> str:
    return 'pokemoncobbled:%s' % item.name.lower().replace('-', '_')
    
async def resolve_evolutions(stat_holder: StatHolder, client: aiopoke.AiopokeClient, aio_species: aiopoke.PokemonSpecies):
    aio_evolution_chain = await client.get_evolution_chain(aio_species.evolution_chain.id)
    if aio_evolution_chain is None or not aio_evolution_chain.chain:
        return
    evolution: Evolution = None
    source_species = clean_species_name(aio_species.name, True)
    try:
        target_chain = aio_evolution_chain.chain if aio_evolution_chain.chain.species.id == aio_species.id else next(filter(lambda chain: chain.species.id == aio_species.id, aio_evolution_chain.chain.evolves_to))
        for chain in target_chain.evolves_to:
            resulting_species = clean_species_name(chain.species.name, True)
            # We need to figure out a way to identify the form of this evolution, PokeAPI is limited on this data
            resulting_property = resulting_species  
            for detail in chain.evolution_details:
                if detail.trigger.name == 'level-up':
                    evolution = PassiveEvolution(source_species, resulting_species, resulting_property)
                elif detail.trigger.name == 'use-item':
                    evolution = ItemInteractionEvolution(source_species, resulting_species, resulting_property, clean_item_name(detail.item))
                elif detail.trigger.name == 'trade':
                    evolution = TradeEvolution(source_species, resulting_species, resulting_property)
                    if detail.trade_species is not None:
                        evolution.trade_partner = clean_species_name(detail.trade_species.name, True)
                # hardcoded due to PokeAPI not specifying damage, only used by Yamask with this exact value
                elif detail.trigger.name == 'take-damage':
                    evolution = PassiveEvolution(source_species, resulting_species, resulting_property, True)
                    evolution.requirements.append(IntRequirement('damage_taken', 49))
                # hardcoded due to PokeAPI only having the one variant, only used by Farfetch'd with this exact value
                elif detail.trigger.name == 'three-critical-hits':
                    evolution = PassiveEvolution(source_species, resulting_species, resulting_property, True)
                    evolution.requirements.append(IntRequirement('battle_critical_hits', 3))
                elif detail.trigger.name.startswith('tower-of-'):
                    evolution = BlockInteractionEvolution(source_species, resulting_species, resulting_property, 'pokemoncobbled:scroll_of_%s' % detail.trigger.name.lower().replace('tower-of-', ''))
                # We need to figure out what to do for this guy
                elif detail.trigger.name == 'spin':
                    evolution = PassiveEvolution(source_species, resulting_species, resulting_property, True)
                # We do nothing for Shedinja we have an event listener in the mod for this gimmick
                elif detail.trigger.name == 'shed':
                    pass
                # If this ever happens we need to update this code
                else:
                    raise ValueError('Failed to determine evolution for %s got evolution type of %s' % (aio_species.name, detail.trigger.name))
                if detail.location is not None:
                    # We have this hardcoded as they do not have a stone alternative yet due to not existing in modern games
                    if stat_holder.pretty_name() == 'nosepass':
                        evolution = ItemInteractionEvolution(evolution.source_species, evolution.resulting_species, evolution.resulting_property, 'pokemoncobbled:thunder_stone')
                    elif stat_holder.pretty_name() == 'crabrawler':
                        evolution = ItemInteractionEvolution(evolution.source_species, evolution.resulting_species, evolution.resulting_property, 'pokemoncobbled:ice_stone')
                    else:
                        print('%s had a location based evolution skipped' % stat_holder.pretty_name())
                if detail.relative_physical_stats is not None:
                    ratio = StatRatio.ATTACK_HIGHER
                    if detail.relative_physical_stats == 0:
                        ratio = StatRatio.EQUAL
                    elif detail.relative_physical_stats == -1:
                        ratio = StatRatio.DEFENCE_HIGHER
                    evolution.requirements.append(StatRatioRequirement(ratio))
                if detail.min_happiness is not None:
                    evolution.requirements.append(IntRequirement('friendship', detail.min_happiness))
                if detail.time_of_day is not None and detail.time_of_day  != '':
                    evolution.requirements.append(TimeRangeRequirement(detail.time_of_day.lower()))
                if detail.held_item is not None:
                    evolution.requirements.append(HeldItemRequirement(clean_item_name(detail.held_item)))
                    evolution.consume_held_item = True
                if detail.party_species is not None:
                    evolution.requirements.append(PartyMemberRequirement(clean_species_name(detail.party_species.name, True)))
                if detail.party_type is not None:
                    evolution.requirements.append(PartyMemberRequirement('type=%s' % detail.party_type.name))
                if detail.min_level is not None:
                    evolution.requirements.append(IntRequirement('level', detail.min_level, 'minLevel'))
                if detail.gender is not None:
                    gender = 'none'
                    if detail.gender == 2:
                        gender = 'male'
                    elif detail.gender == 1:
                        gender = 'female'
                    evolution.requirements.append(PropertiesRequirement('gender=%s' % gender))
                if detail.known_move is not None:
                    evolution.requirements.append(MoveRequirement(clean_move_name(detail.known_move.name)))
                if detail.known_move_type is not None:
                    evolution.requirements.append(MoveTypeRequirement(ElementalType[detail.known_move_type.name.upper()]))
                if detail.needs_overworld_rain is True:
                    evolution.requirements.append(RainRequirement())
                if evolution is not None:
                    if evolution.variant == 'level_up' and len(evolution.requirements) == 0:
                        print('Skipping an empty level up evolution that isn\'t meant to be passive for %s' % stat_holder.pretty_name())
                    else:
                        stat_holder.evolutions.append(evolution)
    except StopIteration:
        return
     
async def scrape_stat_holder(stat_holder: StatHolder, client: aiopoke.AiopokeClient, aio_pokemon: aiopoke.Pokemon, aio_species: aiopoke.PokemonSpecies):
    evolution_task = resolve_evolutions(stat_holder, client, aio_species)
    resolve_types(stat_holder, aio_pokemon)
    resolve_abilities(stat_holder, aio_pokemon)
    resolve_stat_details(stat_holder, aio_pokemon)
    stat_holder.catch_rate = aio_species.capture_rate
    stat_holder.male_ratio = -1 if aio_species.gender_rate == -1 else 1 - aio_species.gender_rate * 1 / 8.0
    stat_holder.base_experience_yield = aio_pokemon.base_experience
    stat_holder.base_friendship = aio_species.base_happiness
    resolve_experience_group(stat_holder, aio_species)
    stat_holder.egg_cycles = aio_species.hatch_counter
    resolve_egg_groups(stat_holder, aio_species)
    resolve_moves(stat_holder, aio_pokemon)
    resolve_labels(stat_holder, aio_species)
    stat_holder.height = aio_pokemon.height
    stat_holder.weight = aio_pokemon.weight
    if aio_species.evolves_from_species is not None:
        stat_holder.pre_evolution = clean_species_name(aio_species.evolves_from_species.name, True)
    await evolution_task   

async def gather_forms(aio_form_pokemon: aiopoke.Pokemon, client: aiopoke.AiopokeClient) -> Iterable[aiopoke.PokemonForm]:
    results = list()
    for resource in aio_form_pokemon.forms:
        form = await client.get_pokemon_form(resource.id)
        results.append(form)
    return results
    
async def resolve_forms(species: Species, client: aiopoke.AiopokeClient, aio_species: aiopoke.PokemonSpecies):
    for variety in aio_species.varieties:
        if variety.is_default == False:
            aio_form_pokemon = await client.get_pokemon(variety.pokemon.id)
            form = Form()
            form.species_name = species.name
            forms = await gather_forms(aio_form_pokemon, client)
            if len(forms) > 1:
                print('%s has more then 1 form' % aio_form_pokemon.name)
            for aio_form in forms:
                if aio_form.is_mega and 'mega_evolution' not in form.labels:
                    form.labels.append('mega_evolution')
                if aio_form.form_name == 'gmax' and 'gmax' not in form.labels:
                    form.labels.append('gmax')
                form.name = aio_form.form_name
            await scrape_stat_holder(form, client, aio_form_pokemon, aio_species)
            species.forms.append(form)  

async def get_fixed_pokemon_species(client: aiopoke.AiopokeClient, name_or_id) -> aiopoke.PokemonSpecies:
    data = await client.http.get(f"pokemon-species/{name_or_id}")
    if data['habitat'] is None:
        data['habitat'] = {
            'name': 'cave',
            'url': 'https://pokeapi.co/api/v2/pokemon-habitat/1/'
        }
    if data['shape'] is None:
        data['shape'] = {
            'name': 'ball',
            'url': 'https://pokeapi.co/api/v2/pokemon-shape/1/'
        }
    # It's okay, it's a Ditto
    if data['evolution_chain'] is None:
        data['evolution_chain'] = { 'url': 'https://pokeapi.co/api/v2/evolution-chain/66/' }
    return aiopoke.PokemonSpecies(**data)

async def scrape_species(national_dex: int, client: aiopoke.AiopokeClient):
    species = Species()
    aio_pokemon = await client.get_pokemon(national_dex)
    # The library has a few bugs with modern Pokémon and not being able to process missing details
    aio_species = await get_fixed_pokemon_species(client, national_dex)
    species.name = clean_species_name(aio_pokemon.name, True)
    species.national_pokedex_number = national_dex
    species_scrape_task = asyncio.create_task(scrape_stat_holder(species, client, aio_pokemon, aio_species))
    forms_scrape_task = asyncio.create_task(resolve_forms(species, client, aio_species))
    await asyncio.wait((species_scrape_task, forms_scrape_task))
    generate_species_json(species, aio_species.generation.id)

async def scrape_technical_disk(machine_id: int, client: aiopoke.AiopokeClient):
    aio_disk = await client.get_machine(machine_id)
    move_name = clean_move_name(aio_disk.move.name)
    raw_item_name = aio_disk.item.name
    disk_type: DiskType = None
    if raw_item_name.startswith('tm'):
        disk_type = DiskType.TM
    elif raw_item_name.startswith('hm'):
        disk_type = DiskType.HM
    elif raw_item_name.startswith('tr'):
        disk_type = DiskType.TR
    else:
        raise ValueError('Cannot determine disk type from the item name %s' % raw_item_name)
    # There should never be more then 1 result
    disk_number = int(raw_item_name.removeprefix(disk_type.value))
    aio_version = await client.get_version_group(aio_disk.version_group.id)
    generation = aio_version.generation.id
    disk = TechnicalDisk(move_name, disk_number, generation, disk_type)
    generate_disk_json(disk)

def generate_species_json(species: Species, generation: int):
    species_path = '/species/generation%s/%s.json' % (generation, species.name)
    export_path = Path('output%s' % species_path)
    append_path = Path('appending%s' % species_path)
    export_path.parent.mkdir(exist_ok=True, parents=True)
    export_data = species.asDict()
    if append_path.exists():
        with open(append_path) as json_file:
            export_data = json.load(json_file) | export_data
    export_path.write_text(json.dumps(obj=export_data, ensure_ascii=False, indent=4))
    print('Generated stat file for: ' + species.name + ' (#' + str(species.national_pokedex_number) + ')')

def generate_disk_json(disk: TechnicalDisk):
    file_name = '%s_%s.json' % (disk.type.value, disk.number)
    export_path = Path('output/technical_disk/generation%s/%s_%s.json' % (disk.generation, disk.type.value, disk.number))
    export_path.parent.mkdir(exist_ok=True, parents=True)
    export_path.write_text(json.dumps(obj=disk.asDict(), ensure_ascii=False, indent=4))
    print('Generated disk file for %s (generation %s)' % (disk.pretty_name(), disk.generation))

async def main():
    async with aiopoke.AiopokeClient() as client:
        print('Scraper is starting...')
        for national_dex in SCRAPE_RANGE: 
            await scrape_species(national_dex, client)
        # Dynamically find latest technical disk
        machine_data = await client.http.get("machine")
        max_machine_id = machine_data['count']
        for machine_id in range(1, max_machine_id + 1):
            await scrape_technical_disk(machine_id, client)
        print('Scraper has finished!')

asyncio.run(main())